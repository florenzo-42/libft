/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_cpy_n.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 4242/42/42 42:42:42 by florenzo          #+#    #+#             */
/*   Updated: 4242/42/42 42:42:42 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		str_cpy_n(char *buff, int n, ...)
{
	va_list		ap;
	char		*str;

	va_start(ap, n);
	while (n-- > 0)
	{
		str = va_arg(ap, char*);
		if (str != NULL)
			while (*str)
				*buff++ = *str++;
	}
	*buff = '\0';
	va_end(ap);
}
