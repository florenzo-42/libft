/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   balloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 17:32:59 by florenzo          #+#    #+#             */
/*   Updated: 2018/05/28 17:32:59 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*balloc(void *ptr, size_t old_size, size_t new_size)
{
	void *new;

	new = ealloc(new_size);
	ft_memmove(new, ptr, old_size);
	return (new);
}
