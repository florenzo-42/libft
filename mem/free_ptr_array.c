/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_ptr_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 4242/42/42 42:42:42 by florenzo          #+#    #+#             */
/*   Updated: 4242/42/42 42:42:42 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	free_ptr_array(void **tab)
{
	void	**ptr;

	if (tab != NULL)
	{
		ptr = tab;
		while (*ptr != NULL)
			free(*ptr++);
	}
	free(tab);
}
