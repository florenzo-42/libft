/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   double_buffer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 4242/42/42 42:42:42 by florenzo          #+#    #+#             */
/*   Updated: 4242/42/42 42:42:42 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		double_buffer(void **ptr_address, int *size)
{
	void	*tmp;

	tmp = balloc(*ptr_address, *size, *size * 2);
	free(*ptr_address);
	*ptr_address = tmp;
	*size *= 2;
}
