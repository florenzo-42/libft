# Libft
This is the first C project at 42. We have to recode many standard functions (prefixed with `ft_`) and we are invited to expand it with functions we found useful during other projects.  
Since this is for personal use there is very little documentation, if the name isn't explicit enough the code should be easily understandable.
### Build
If i included this in a project the Makefile will contain a rule to build it, anyway :
```
git clone http://gitlab.com/florenzo-42/libft
cd libft
make
```